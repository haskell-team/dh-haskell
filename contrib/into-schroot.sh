#!/bin/sh
# Copyright (C) 2015  Bogatov Dmitry <KAction@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Build dh-hashell package and install it into schroot, to make possible to
# build packages, that depends on it.
#
# The right thing to do is setup repository, external to schroot, but in case
# of single package, I find it easier to peek /var/lib/schroot directly.

if [ ! -f debian/control ] ; then
   echo 2>&1 "Run from root of repository"
   exit 1
fi

set -e

: ${SCHROOT_NAME:=sid-amd64}
PACKAGE=dh-haskell_$(dpkg-parsechangelog -SVersion)_all.deb

sbuild -c "$SCHROOT_NAME"
sudo cp -f ../"$PACKAGE" "/var/lib/schroot/chroots/$SCHROOT_NAME/tmp"
sudo schroot -c "source:$SCHROOT_NAME" --directory / -- dpkg -i "/tmp/$PACKAGE"
