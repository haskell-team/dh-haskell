#!/bin/sh
git config alias.root '!pwd'
GIT_DIR=$(git root)/.git

cat << 'EOF' > "$GIT_DIR/hooks/pre-commit"
#!/bin/sh
set -eu
$GIT_DIR/../contrib/check_perltidy
EOF

chmod +x "$GIT_DIR/hooks/pre-commit"
