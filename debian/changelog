dh-haskell (0.6.6) unstable; urgency=medium

  * Set explicitly the location for GHC's temporary package database.
  * Do not install automatic Lintian overrides; will use Lintian's screens
    instead.

 -- Felix Lechner <felix.lechner@lease-up.com>  Wed, 13 Apr 2022 12:28:47 -0700

dh-haskell (0.6.5) unstable; urgency=medium

  * Make sure to insert spaces when appending command-line options to
    environment variables. (undocumented Haddock/MathJax bug)

 -- Felix Lechner <felix.lechner@lease-up.com>  Mon, 11 Apr 2022 13:05:08 -0700

dh-haskell (0.6.4) unstable; urgency=medium

  * Use new integrated install_recipe() adapted from the CDBS module
    hlibrary.mk from haskell-devscripts. (Closes: #865640)

 -- Felix Lechner <felix.lechner@lease-up.com>  Mon, 11 Apr 2022 04:42:40 -0700

dh-haskell (0.6.3) unstable; urgency=medium

  * Use the same build recipes as the older CDBS module hlibrary.mk
    from haskell-devscripts.

 -- Felix Lechner <felix.lechner@lease-up.com>  Fri, 08 Apr 2022 16:32:21 -0700

dh-haskell (0.6.2) unstable; urgency=medium

  * Use Perl recipes directly instead of going through the Dh_Haskell.sh
    shell library.
  * Invoke build time tests ("make check") when appropriate.
  * Call Haddock after the build to generate documentation.
  * Use run() facility from build system instead of IPC::Run3.
  * Run bullseye-backports in Salsa CI instead of buster-backports.
  * Bump Debhelper level to 13.

 -- Felix Lechner <felix.lechner@lease-up.com>  Thu, 07 Apr 2022 07:33:33 -0700

dh-haskell (0.6.1) unstable; urgency=medium

  * Provide dh-sequence-haskell in the installable. (Closes: #1005893)

 -- Felix Lechner <felix.lechner@lease-up.com>  Tue, 22 Feb 2022 10:39:52 -0800

dh-haskell (0.6) unstable; urgency=medium

  * Release into unstable.
  * Drop installation prerequisite File::Find::Rule from build system.

 -- Felix Lechner <felix.lechner@lease-up.com>  Tue, 22 Feb 2022 09:56:39 -0800

dh-haskell (0.5) experimental; urgency=medium

  * Simple, but nearly complete rewrite based on the currently preferred
    alternative /usr/share/cdbs/1/class/hlibrary.mk from haskell-devscripts.
    (Closes: #1002296)
  * New maintainer.
  * Fix typo in d/copyright to clarify choice of GPL-3+.
  * Adjust build and runtime prerequisites to new code base.
  * Bump Debhelper compat level to 10.
  * Bump Standards-Version to 4.6.0.
  * Add d/salsa-ci.yml for standard Salsa CI pipeline.
  * Add perltidy and perlcritic configurations.
  * Add *.bak and *.rej to .gitignore.

 -- Felix Lechner <felix.lechner@lease-up.com>  Tue, 21 Dec 2021 14:32:04 -0800

dh-haskell (0.4) experimental; urgency=low

  * Use ghc-pkg to construct virtual package names instead of manual
    parsing.

 -- Sven Bartscher <kritzefitz@debian.org>  Sun, 09 Oct 2016 13:36:21 +0200

dh-haskell (0.3.1) unstable; urgency=medium

  * Take advantage of ${perl:Depends} variable, provided by dh_perl(1).
    (Closes: #839020) (Thanks: Niko Tyni <ntyni@debian.org>)

 -- Dmitry Bogatov <KAction@gnu.org>  Wed, 28 Sep 2016 19:27:54 +0300

dh-haskell (0.3) unstable; urgency=medium

  * Avoid push on scalar, which in now forbidden (Closes: #838888)
    (Thanks: Chris Lamb <lamby@debian.org>)
  * Replace git:// with https:// link in Vcs-Git

 -- Dmitry Bogatov <KAction@gnu.org>  Mon, 26 Sep 2016 10:44:07 +0300

dh-haskell (0.2) unstable; urgency=medium

  * Correctly handle packages without library. (Closes: #820406)
    Patch is inspired by one, provided by Neil Mayhew.
  * Fix handling of multiple flags, returned by `dpkg-buildflags'.
    (Closes: #822947) Patch provided by Colin Watson (cjwatson@ubuntu.com).
  * Bump standards version to 3.9.8 (no changes needed)
  * Fix debian/copyright syntax
  * Use secure uri in Vcs-Git field
  * Fix spelling errors
  * Fix wrong links in debian/control

 -- Dmitry Bogatov <KAction@gnu.org>  Fri, 08 Apr 2016 12:47:00 +0300

dh-haskell (0.1) unstable; urgency=low

  * Initial release

 -- Dmitry Bogatov <KAction@gnu.org>  Thu, 09 Jul 2015 23:27:17 +0300
