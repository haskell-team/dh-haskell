#!/usr/bin/perl
#
# A Debhelper sequencing module for cabalized Haskell packages.
#
# Copyright © 2015  Bogatov Dmitry <KAction@gnu.org>
# Copyright © 2021  Felix Lechner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use v5.20;
use warnings;
use utf8;

use Debian::Debhelper::Dh_Lib;

use constant USE_LEGACY => 1;

add_command_options('dh_makeshlibs', '--noscripts', '-XlibHS');
add_command_options('dh_shlibdeps', '-u -xlibgmp10 --ignore-missing-info');

if (USE_LEGACY) {
    add_command_options('dh_shlibdeps', '-XlibHS');
}

if (USE_LEGACY) {
    add_command_options('dh_gencontrol',
        '-u-DGHC-Package=${haskell:ghc-package}');
}

# We must forbid debhelper compress interface description files,
# otherwise hoogle would be unhappy. FIXME: It it true?
add_command_options('dh_compress', '-X.txt');

insert_after('dh_installchangelogs', 'dh_buildinfo');

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
